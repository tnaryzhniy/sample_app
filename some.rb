def string_shuffle(s)
  s.split('').shuffle.join
end

puts string_shuffle("foobar")

person1 = { first: "first name", last: "last name" }
person2 = { first: "first name", last: "last name" }
person3 = { first: "first name", last: "last name" }

params = { father: person1, mother: person2, child: person3 }

puts params[:father]
puts params[:father][:first]


Using rake 10.4.2
Using i18n 0.7.0
Using json 1.8.2
Using minitest 5.6.0
Using thread_safe 0.3.5
Using tzinfo 1.2.2
Using activesupport 4.2.0
Using builder 3.2.2
Using erubis 2.7.0
Using mini_portile 0.6.2
Using nokogiri 1.6.6.2
Using rails-deprecated_sanitizer 1.0.3
Using rails-dom-testing 1.0.6
Using loofah 2.0.1
Using rails-html-sanitizer 1.0.2
Using actionview 4.2.0
Using rack 1.6.0
Using rack-test 0.6.3
Using actionpack 4.2.0
Using globalid 0.3.5
Using activejob 4.2.0
Using mime-types 2.4.3
Using mail 2.6.3
Using actionmailer 4.2.0
Using activemodel 4.2.0
Using arel 6.0.0
Using activerecord 4.2.0
Using debug_inspector 0.0.2
Using binding_of_caller 0.7.2
Using bundler 1.8.5
Using columnize 0.9.0
Using byebug 4.0.5
Installing coffee-script-source 1.9.1.1
Using execjs 2.5.2
Using coffee-script 2.4.1
Using thor 0.19.1
Using railties 4.2.0
Using coffee-rails 4.1.0
Using multi_json 1.11.0
Using jbuilder 2.2.13
Using jquery-rails 4.0.3
Using sprockets 3.0.1
Using sprockets-rails 2.2.4
Using rails 4.2.0
Using rdoc 4.2.0
Using sass 3.4.13
Using tilt 1.4.1
Using sass-rails 5.0.3
Using sdoc 0.4.1
Using sqlite3 1.3.10
Using turbolinks 2.5.3
Using tzinfo-data 1.2015.3
Using uglifier 2.7.1
Using web-console 2.1.2